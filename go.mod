module gitlab.com/micro-test/monorepo-test

go 1.13

require (
	github.com/golang/protobuf v1.4.3
	github.com/micro/go-micro/v3 v3.0.0-beta.2.0.20200921154545-9dbd75f2cc13
	github.com/micro/micro/v3 v3.0.0-beta.7
	google.golang.org/grpc v1.27.0
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0
